console.log("Hello");
/* 1. Customer class:

email property - string

cart property - instance of Cart class

orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty
 
====
2. Product class:

name property - string

price property - number

isActive property - Boolean: defaults to true

archive() method - will set isActive to false if it is true to begin with

updatePrice() method - replaces product price with passed in numerical value


3. Cart class:

contents property - array of objects with structure: {product: instance of Product class, quantity: number}

totalAmount property - number

addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

showCartContents() method - logs the contents property in the console

updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

clearCartContents() method - empties the cart contents

computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.

*/

class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }

  checkOut() {
    // this.cart.computeTotal();

    this.orders.push(this.cart);
    return this;
  }
}

//====================//
class Product {
  constructor(name, price) {
    this.name = name;
    this.price = price;
    this.isActive = true;
  }
  archive() {
    this.isActive = false;
    return this;
  }

  updatePrice(price) {
    this.price = price;
    return this;
  }
}

//=====================//
class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }
  addToCart(product, quantity) {
    this.contents.push({ product: product, quantity: quantity });
    return this;
  }
  showCartContents() {
    console.log(this.contents);
  }
  updateProductQuantity(product, quantity) {
    this.contents.forEach(elem => {
      if (elem.product.name === product) {
        elem.quantity = quantity;
        this.computeTotal();
      }
    });
    return this;
  }
  clearCartContents() {
    this.contents = [];
    this.totalAmount = 0;
    return this;
  }
  computeTotal() {
    let sum = 0;
    this.contents.forEach(elem => {
      sum += elem.product.price * elem.quantity;
    });
    this.totalAmount = sum;
    return this;
  }
}

//test statements
const john = new Customer("john@mail.com");

const prodA = new Product("soap", 9.99);
const prodB = new Product("shampoo", 12.99);
const prodC = new Product("toothbrush", 4.99);
const prodD = new Product("toothpaste", 14.99);

john.cart.addToCart(prodA, 3);
john.cart.addToCart(prodB, 2);

// john.cart.computeTotal()
// john.cart.updateProductQuantity('soap', 5)
// john.cart.showCartContents()
// john.cart.clearCartContents()
// john.cart.showCartContents()
// john.checkOut()
